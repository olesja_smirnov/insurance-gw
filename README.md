# insurance-gateway

# Getting Started
Set SDK for project (project compiled with 13)

## IDEA configuration
Install plugin "Lombok" to your editor (for older IDEA version)

## API documentation
* Open API doc via Swagger http://localhost:8081/swagger-ui/

## DB CONFIGURATION (if without docker)
DriverClassName: jdbc:h2:file:~/casco;DB_CLOSE_ON_EXIT=FALSE

## Task description
**Car casco insurance**

1. Build a CLI app to calculate car casco payment and write results to file. Take into account only risk coefficients vehicle age, make and current value:
    * annual and monthly fee;
    * all car producers have a make_risk of 1, if not listed in data.json (data.make_coefficients);
    * if vehicle producer is not listed in average purchase price data, skip calculations
2. Repeat step 1 and calculate car casco payment, when previous indemnity is added as risk;
3. Import original datasets into in memory database:
    * Based on db data repeat step 1 and save results in db;
4. Convert CLI app to web app and display:
    * all available risks. Highlight risks, which are included in calculation;
    * calculated data: id, annual payment, car producer, year of production, mileage, previous indemnity;
5. Add check-box that can change view:
    * If check-box is checked, add to view monthly payment column;
6. Group results showing only 10 results per page;
7. Add check-box that takes into account whether indemnity risk is used in casco payment calculation or not;
8. Create a functionality to sort results by column name;
9. CRUD for managing cars:
    * functionality to modify or delete cars in list;
    * functionality to add new cars;
10. CRUD for managing risks:
    * functionality to modify or delete risk-parameter key-pair in list;
    * functionality to add new risk to formula and take it into account during calculation;
11. Implement unit tests;

## Initial data provided:
1. Datasets:
    * vehicles.csv;
    * data.json;

2. Formulas:
    * formulas.md;