package casco;

import static casco.errors.ErrorMsg.ATTEMPT;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import casco.service.RunnableService;

@SpringBootApplication
@Slf4j
public class InsuranceGwApplication implements CommandLineRunner {

	@Autowired
	private RunnableService runnableService;

	public static void main(String[] args) {
		SpringApplication.run(InsuranceGwApplication.class, args);
	}


	@Override
	public void run(String... args) {
		log.info(ATTEMPT + "Running Scheduler service for saving Vehicles data to database...");
		runnableService.downloadVehicleData();
	}

}
