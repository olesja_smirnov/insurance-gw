package casco.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import casco.domain.model.Vehicle;
import casco.domain.repo.VehicleRepository;

@Service
public class CascoService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Transactional
    public void saveVehiclesFromCsvFile(List<Vehicle> vehicles) {
        vehicleRepository.saveAll(vehicles);
    }

}