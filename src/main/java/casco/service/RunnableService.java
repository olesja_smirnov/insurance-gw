package casco.service;

import static casco.errors.ErrorMsg.ATTEMPT;
import static casco.errors.ErrorMsg.ERROR;
import static casco.errors.ErrorMsg.SUCCESS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import casco.domain.model.Vehicle;

@Service
@Slf4j
public class RunnableService {

    private static final String CSV_DELIMITER = ",";
    private static final String FILE_PATH = "vehicles.csv";

    @Autowired
    private CascoService cascoService;

    @Transactional
    public void downloadVehicleData() {

        new Thread(() -> {
            try {
                List<Vehicle> downloadedVehicles = insertCsvToVehicle();
                cascoService.saveVehiclesFromCsvFile(downloadedVehicles);
            } catch (Exception e) {
                log.warn("Vehicle data migration failed", e);
                e.printStackTrace();
            }
        }).start();
    }

    @Transactional
    List<Vehicle> insertCsvToVehicle() throws Exception {
        log.info(ATTEMPT + "Starting to extract Vehicles data from file.");

        ClassPathResource classPathResource = new ClassPathResource(FILE_PATH, this.getClass().getClassLoader());

        BufferedReader br = new BufferedReader(new InputStreamReader(classPathResource.getInputStream(), StandardCharsets.UTF_8));
        Stream<String> row = br.lines();
        // skip header row
        row = row.skip(1);

        AtomicInteger counter = new AtomicInteger();
        log.info(ATTEMPT + "Starting to insert vehicles to db");

        List<Vehicle> vehicles = new ArrayList<>();
        row.forEach(vehicle -> {
            String[] split = vehicle.split(CSV_DELIMITER);
            counter.incrementAndGet();
            log.info("Processing row nr {}...", counter.get());

            Vehicle downloadedVehicle = getVehicleBuilderFromCsv(split);
            if (downloadedVehicle != null) {
                vehicles.add(downloadedVehicle);
            }
        });

        br.close();
        log.info(SUCCESS + "File is processed with {} vehicles lines.", counter.get());

        return vehicles;
    }

    private Vehicle getVehicleBuilderFromCsv(String[] split) {
        if (split.length != 7 ) {
            return null;
        }
        try {
            return Vehicle.builder()
                    .plateNumber(split[1])
                    .firstRegistration(getIntegerValue(split[2]))
                    .purchasePrice(getBigDecimalValue(split[3]))
                    .producer(split[4])
                    .mileage(getIntegerValue(split[5]))
                    .previousIndemnity(getDoubleValue(split))
                    .build();
        } catch (NumberFormatException e) {
            log.error(ERROR + "Failed to convert csv row values to requested type.");
            return null;
        }
    }

    private BigDecimal getBigDecimalValue(String value) {
        if (value.isBlank()) {
            return null;
        }

        boolean numericValue = matchValue(value);
        if (numericValue) {
            return new BigDecimal(value);
        }
        String replacedValue = value.replace("O","0");

        return new BigDecimal(replacedValue);
    }

    private Integer getIntegerValue(String value) {
        if (value.isBlank()) {
            return null;
        }

        boolean numericValue = matchValue(value);
        if (numericValue) {
            return Integer.parseInt(value);
        }
        String replacedValue = value.replace("O","0");

        return Integer.parseInt(replacedValue);
    }

    private Double getDoubleValue(String[] value) {
        String param = value[6];
        if (param.isBlank()) {
            return null;
        }

        param = checkIfIndemnityContainsMoreDots(param);

        boolean numericValue = matchValue(param);
        if (numericValue) {
            return Double.parseDouble(param);
        }
        String replacedValue = param.replace("O","0");

        return Double.parseDouble(replacedValue);
    }

    private boolean matchValue(String value) {
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    private String checkIfIndemnityContainsMoreDots(String param) {
        if (param.contains("..")) {
            param = param.replace(".", "");
        }
        return param;
    }

}